import cv2 as cv
import numpy as np

# Leer imagen cuadrado
img1 = cv.imread('./cuadrado.jpg')
# Leer imagen elipse
img2 = cv.imread('./elipse_inv.jpg')

#Intica el tamaño de la imagen
print(img1.shape)
print(img2.shape)

imagen1_res = cv.resize(img1, dsize=(412, 412), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(img2, dsize=(412, 412), interpolation=cv.INTER_CUBIC)

#AND
res = cv.bitwise_and(imagen1_res,imagen2_res)
#NOT
#res = cv.bitwise_not(imagen2_res)
#res = cv.bitwise_not(imagen1_res)
#OR
#res = cv.bitwise_or(imagen1_res,imagen2_res)
#XOR
#res = cv.bitwise_xor(imagen1_res,imagen2_res)

# Realizar operaciones solicitadas
cv.imshow('Cuadrado', imagen1_res)
cv.imshow('Elipse', imagen2_res)
cv.imshow('Opercaion', res)

cv.waitKey(0)
cv.destroyWindow()